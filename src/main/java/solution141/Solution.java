package solution141;

/**
 * Definition for singly-linked list.
 * class ListNode {
 * int val;
 * ListNode next;
 * ListNode(int x) {
 * val = x;
 * next = null;
 * }
 * }
 */
public class Solution {
    public boolean hasCycle(ListNode head) {
        if (head == null) {
            return false;
        }
        ListNode firs = head;
        ListNode second = head;
        while (true) {
            firs = firs.next;
            second = second.next != null ? second.next.next : null;
            if (firs == null || second == null) {
                return false;
            } else if (firs == second) {
                return true;
            }
        }
    }


    static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
            next = null;
        }
    }
}