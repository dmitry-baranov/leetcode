package solution71;

import java.util.Stack;

class Solution {
    public String simplifyPath(String path) {
        String[] steps =  path.split("/");
        Stack<String> canonicalPath = new Stack<String>();
        for (String step : steps) {
            if (step.equals("..") && !canonicalPath.empty()) {
                canonicalPath.pop();
            } else //noinspection StatementWithEmptyBody
                if (step.equals(".") || step.equals("") || step.equals("..")) {
                } else {
                    canonicalPath.push(step);
                }
        }
        return "/" + String.join("/", canonicalPath);
    }
}