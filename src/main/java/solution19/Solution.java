package solution19;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 * int val;
 * ListNode next;
 * ListNode() {}
 * ListNode(int val) { this.val = val; }
 * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {

    public static void main(String[] args) {
        ListNode head = new ListNode(1, new ListNode(2, null));
        removeNthFromEnd(head, 2);
    }

    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode currentNode = head;
        int sizeList = 0;
        while (currentNode != null) {
            sizeList++;
            currentNode = currentNode.next;
        }
        if (sizeList == 1 && n == 1) {
            return null;
        }
        if (sizeList - n - 1 == -1) {
            assert head != null;
            return head.next;
        }
        int i = 0;
        currentNode = head;
        while (i != (sizeList - n - 1)) {
            i++;
            currentNode = currentNode.next;
        }
        currentNode.next = currentNode.next.next;
        return head;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
