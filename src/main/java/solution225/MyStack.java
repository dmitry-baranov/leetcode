package solution225;

import java.util.LinkedList;
import java.util.List;

class MyStack {

    private List<Integer> firstList;
    private List<Integer> secondList;
    private Integer top;

    public MyStack() {
        firstList = new LinkedList<>();
        secondList = new LinkedList<>();
    }

    public void push(int x) {
        firstList.add(x);
        top = x;
    }

    public int pop() {
        int removedElement = top;
        while (firstList.size() > 1) {
            top = firstList.remove(0);
            secondList.add(top);
        }
        if (secondList.isEmpty()) {
            top = null;
        }
        firstList.remove(0);
        List<Integer> temp = firstList;
        firstList = secondList;
        secondList = temp;
        return removedElement;
    }

    public int top() {
        return top;
    }

    public boolean empty() {
        return firstList.isEmpty();

    }
}

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack obj = new MyStack();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.top();
 * boolean param_4 = obj.empty();
 */