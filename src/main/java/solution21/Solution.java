package solution21;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {

    public static void main(String[] args) {
        ListNode list1 = new ListNode(1, new ListNode(2, new ListNode(4, null)));
        ListNode list2 = new ListNode(1, new ListNode(3, new ListNode(4, null)));
        mergeTwoLists(list1, list2);
    }

    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null && list2 == null) {
            return null;
        }
        if (list1 == null) {
            return list2;
        }
        if (list2 == null) {
            return list1;
        }
        ListNode head;
        ListNode currentNode;
        if (list1.val > list2.val) {
            head = list2;
            currentNode = list2;
        } else {
            head = list1;
            currentNode = list1;
        }
        while (list1.next != null || list2.next != null) {
            if (list1.next == null) {
                currentNode.next = list2.next;
                list2 = list2.next;
            } else if (list2.next == null) {
                currentNode.next = list1.next;
                list1 = list1.next;
            } else if (list1.val < list2.val) {
                currentNode.next = list1.next;
                list1 = list1.next;
            } else {
                currentNode.next = list2.next;
                list2 = list2.next;
            }
            currentNode = currentNode.next;
        }
        currentNode.next = null;
        return head;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}