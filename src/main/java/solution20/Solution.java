package solution20;

import java.util.Stack;

class Solution {
    public boolean isValid(String s) {
        char[] chars = s.toCharArray();
        Stack<Character> stackBrackets = new Stack<Character>();
        for (char c : chars) {
            if (isOpen(c)) {
                stackBrackets.push(c);
            } else {
                if (!stackBrackets.empty() && isReserveBracket(stackBrackets.peek(), c)) {
                    stackBrackets.pop();
                } else {
                    return false;
                }
            }
        }
        return stackBrackets.empty();
    }

    private boolean isOpen(Character c) {
        return c == '{' || c == '(' || c == '[';
    }

    private boolean isReserveBracket(Character source, Character target) {
        if (source == '(') {
            return target == ')';
        }
        if (source == '[') {
            return target == ']';
        }
        if (source == '{') {
            return target == '}';
        }
        return false;
    }
}